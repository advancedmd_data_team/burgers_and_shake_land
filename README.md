# Burgers and Shakeland

This repository contains data regarding a fictional Restaurant chain "Burgers
and Shakeland". Use the data to solve the following questions.You may do so in
SQL, excel, python or another tool that you would like. Please email your
answers and any supporting calculations/spreadsheets/SQL queries you would like
to your recruiter.

Questions:

1. Which location is the busiest? Does the answer to this question change
   throughout the day?
2. Which three (3) employees had the highest percent increase to their monthly
   sales from March to April? What were their percent increases?
3. A combo meal consists of 1 Burger, 1 Fry, and either a Drink or a Shake. How
   many combo meals were sold from the State Street location in April?

Bonus: some of the employees have been complaining about working long hours.
Based only on the transaction data, can you tell if any employees are working
more than a typical 8 hour shift?


## Getting Started.

Inside the data folder you will find two files. These files contain the same data so you only need to
use one to answer the questions.

**burgers_and_shakeland.csv**: This file contains the data in a csv format
for use with Excel or other program that can ingest .csv files

**burgers_and_shakeland.db**: This is a Sqlite Database that can be used to query the data
using SQL. It contains one table named `sales`.


